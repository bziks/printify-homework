# Setup
### Install docker container
```bash
docker-compose up -d
```
### Composer install
```bash
docker exec -it printify_homework_web bash
cd /var/www/html
composer install
```
### Create DB tables
```bash
./bin/console doctrine:migrations:migrate
```
### Fill DB tables
```bash
./bin/console doctrine:fixtures:load
```

# API
All responses come in standard JSON. All requests must include a content-type of application/json and the body must be valid JSON.

OpenAPI specifications: http://localhost:8080/api/doc.json