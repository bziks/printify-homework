<?php

namespace App\Controller;

use App\Service\TransferObjectSerializer;
use FOS\RestBundle\Controller\AbstractFOSRestController;


class AbstractController extends AbstractFOSRestController
{
    /** @var TransferObjectSerializer */
    protected $serializer;

    /**
     * AbstractController constructor.
     * @param TransferObjectSerializer $serializer
     */
    public function __construct(TransferObjectSerializer $serializer)
    {
        $this->serializer = $serializer;
    }
}
