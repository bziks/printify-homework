<?php

namespace App\Controller;

use App\DTO\Input\AddressInputDTO;
use App\Service\AddressService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityNotFoundException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;


class AddressController extends AbstractController
{

    /**
     * Add address to user
     *
     * @Rest\Post("/user/{userId}/address")
     *
     * @SWG\Tag(name="add-address")
     *
     * @SWG\Parameter(
     *     name="Message body",
     *     in="body",
     *     type="string",
     *     description="JSON string",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="fullName", type="string"),
     *         @SWG\Property(property="address", type="string"),
     *         @SWG\Property(property="country", type="string"),
     *         @SWG\Property(property="state", type="string|null"),
     *         @SWG\Property(property="city", type="string"),
     *         @SWG\Property(property="zip", type="string|null"),
     *         @SWG\Property(property="phone", type="string"),
     *         @SWG\Property(property="region", type="string|null")
     *     )
     * )
     * @SWG\Response(
     *     response="201",
     *     description="Return the address id",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="userId", type="integer")
     *     )
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Any errors",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string"),
     *     )
     * )
     * @SWG\Response(
     *     response="404",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="User not found"),
     *     )
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Internal Service Exception",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="Some error"),
     *     )
     * )
     *
     *
     * @param AddressService $addressService
     * @param Request $request
     * @param int $userId
     * @return View
     * @throws EntityNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function postAddress(
        AddressService $addressService,
        Request $request,
        int $userId
    ): View
    {
        /** @var AddressInputDTO $addressInputDTO */
        $addressInputDTO = $this->serializer->deserializeJson($request->getContent(), AddressInputDTO::class);

        $addressCreatedDTO = $addressService->create($userId, $addressInputDTO);

        return View::create($addressCreatedDTO, Response::HTTP_CREATED);
    }


}
