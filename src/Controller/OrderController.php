<?php

namespace App\Controller;

use App\Service\OrderService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;


class OrderController extends AbstractController
{
    /**
     * Create order
     *
     * @Rest\Post("/user/{userId}/order")
     *
     * @SWG\Tag(name="create-order")
     *
     * @SWG\Parameter(
     *     name="Message body",
     *     in="body",
     *     type="string",
     *     description="JSON string",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="...", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response="201",
     *     description="...",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="...", type="integer")
     *     )
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Any errors",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string"),
     *     )
     * )
     * @SWG\Response(
     *     response="404",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="User not found"),
     *     )
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Internal Service Exception",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="Some error"),
     *     )
     * )
     *
     * @param OrderService $orderService
     * @param Request $request
     * @param int $userId
     * @return View
     */
    public function postOrder(
        OrderService $orderService,
        Request $request,
        int $userId
    ): View
    {

    }

    /**
     * Set shipping for order
     *
     * @Rest\Post("/user/{userId}/order/shipping")
     *
     * @SWG\Tag(name="set-shipping")
     *
     * @SWG\Parameter(
     *     name="Message body",
     *     in="body",
     *     type="string",
     *     description="JSON string",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="...", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response="201",
     *     description="...",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="...", type="integer")
     *     )
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Any errors",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string"),
     *     )
     * )
     * @SWG\Response(
     *     response="404",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="User not found"),
     *     )
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Internal Service Exception",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="Some error"),
     *     )
     * )
     *
     * @param OrderService $orderService
     * @param Request $request
     * @param int $userId
     * @return View
     */
    public function orderShipping(
        OrderService $orderService,
        Request $request,
        int $userId
    ): View
    {
        
    }

    /**
     * Order payment
     *
     * @Rest\Post("/user/{userId}/order/payment")
     *
     * @SWG\Tag(name="order-payment")
     *
     * @SWG\Parameter(
     *     name="Message body",
     *     in="body",
     *     type="string",
     *     description="JSON string",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="...", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response="201",
     *     description="...",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="...", type="integer")
     *     )
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Any errors",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string"),
     *     )
     * )
     * @SWG\Response(
     *     response="404",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="User not found"),
     *     )
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Internal Service Exception",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="Some error"),
     *     )
     * )
     *
     * @param OrderService $orderService
     * @param Request $request
     * @param int $userId
     * @return View
     */
    public function orderPayment(
        OrderService $orderService,
        Request $request,
        int $userId
    ): View
    {

    }

    /**
     * Order list by user
     *
     * @Rest\Get("/user/{userId}/orders")
     *
     * @SWG\Tag(name="order-list")
     *
     * @SWG\Parameter(
     *     name="Message body",
     *     in="body",
     *     type="string",
     *     description="JSON string",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="...", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response="201",
     *     description="...",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="...", type="integer")
     *     )
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Any errors",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string"),
     *     )
     * )
     * @SWG\Response(
     *     response="404",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="User not found"),
     *     )
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Internal Service Exception",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="Some error"),
     *     )
     * )
     *
     * @param OrderService $orderService
     * @param Request $request
     * @param int $userId
     * @return View
     */
    public function orderList(
        OrderService $orderService,
        Request $request,
        int $userId
    ): View
    {

    }
}
