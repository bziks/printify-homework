<?php

namespace App\Controller;

use App\DTO\Input\ProductInputDTO;
use App\Service\ProductService;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ProductController extends AbstractController
{
    /**
     * Add product to user
     *
     * @Rest\Post("/user/{userId}/product")
     *
     * @SWG\Tag(name="add-product")
     *
     * @SWG\Parameter(
     *     name="Message body",
     *     in="body",
     *     type="string",
     *     description="JSON string",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="title", type="string"),
     *         @SWG\Property(property="sku", type="string"),
     *         @SWG\Property(property="cost", type="float"),
     *         @SWG\Property(property="productType", type="string", enum={"mug", "tshirt"})
     *     )
     * )
     * @SWG\Response(
     *     response="201",
     *     description="Return the product id",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="userId", type="integer")
     *     )
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Something does not exist",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string"),
     *     )
     * )
     * @SWG\Response(
     *     response="400",
     *     description="Any errors",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string"),
     *     )
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Internal Service Exception",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="Some error"),
     *     )
     * )
     *
     * @param ProductService $productService
     * @param Request $request
     * @param int $userId
     * @return View
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function postProduct(
        ProductService $productService,
        Request $request,
        int $userId
    ): View
    {
        /** @var ProductInputDTO $productDTO */
        $productDTO = $this->serializer->deserializeJson($request->getContent(), ProductInputDTO::class);

        $product = $productService->create($userId, $productDTO);

        return View::create($product, Response::HTTP_CREATED);
    }

    /**
     * Get product list by user
     *
     * @Rest\Get("/user/{userId}/products")
     *
     * @SWG\Tag(name="get-user-products")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Return the product list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="title", type="string"),
     *              @SWG\Property(property="sku", type="string"),
     *              @SWG\Property(property="cost", type="float"),
     *              @SWG\Property(property="productType", type="string", enum={"Mug", "T-shirt"})
     *          )
     *     )
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="User not found"),
     *     )
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Internal Service Exception",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="Some error"),
     *     )
     * )
     *
     * @param ProductService $productService
     * @param int $userId
     * @return View
     * @throws EntityNotFoundException
     */
    public function productList(
        ProductService $productService,
        int $userId
    ): View
    {
        $products = $productService->getByUserId($userId);

        return View::create($products, Response::HTTP_OK);
    }
}
