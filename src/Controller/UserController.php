<?php

namespace App\Controller;

use App\DTO\Input\UserInputDTO;
use App\Service\UserService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UserController extends AbstractController
{
    /**
     * Create user.
     *
     * @Rest\Post("/user")
     *
     * @SWG\Tag(name="create-user")
     *
     * @SWG\Parameter(
     *     name="Message body",
     *     in="body",
     *     type="string",
     *     description="JSON string",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="name", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response="201",
     *     description="Return the user id",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="userId", type="integer")
     *     )
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Any errors",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string"),
     *     )
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Internal Service Exception",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(property="code", type="integer"),
     *         @SWG\Property(property="message", type="string", description="Some error"),
     *     )
     * )
     *
     * @param UserService $userService
     * @param Request $request
     * @return View
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function postUser(
        UserService $userService,
        Request $request
    ): View
    {
        /** @var UserInputDTO $userDTO */
        $userDTO = $this->serializer->deserializeJson($request->getContent(), UserInputDTO::class);

        $userOutputDTO = $userService->create($userDTO);

        return View::create($userOutputDTO, Response::HTTP_CREATED);
    }
}
