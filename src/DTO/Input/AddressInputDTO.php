<?php

namespace App\DTO\Input;

use App\Entity\Country;


class AddressInputDTO
{
    /** @var string */
    private $fullName;
    /** @var string */
    private $address;
    /** @var Country */
    private $country;
    /** @var ?string */
    private $state;
    /** @var string */
    private $city;
    /** @var ?string */
    private $zip;
    /** @var string */
    private $phone;
    /** @var ?string */
    private $region;

    /**
     * AddressInputDTO constructor.
     * @param string $fullName
     * @param string $address
     * @param Country $country
     * @param string $city
     * @param string $phone
     * @param string|null $region
     * @param string|null $state
     * @param string|null $zip
     */
    public function __construct(
        string $fullName,
        string $address,
        Country $country,
        string $city,
        string $phone,
        ?string $region,
        ?string $state,
        ?string $zip
    ) {
        $this->fullName = $fullName;
        $this->address = $address;
        $this->country = $country;
        $this->state = $state;
        $this->city = $city;
        $this->zip = $zip;
        $this->phone = $phone;
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

}
