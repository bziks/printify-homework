<?php

namespace App\DTO\Input;

use App\Entity\ProductType;
use App\Entity\User;


class ProductInputDTO
{
    /** @var string */
    private $title;
    /** @var string */
    private $sku;
    /** @var string */
    private $cost;
    /** @var ProductType */
    private $productType;

    /**
     * ProductDTO constructor.
     * @param string $title
     * @param string $sku
     * @param string $cost
     * @param ProductType $productType
     */
    public function __construct(string $title, string $sku, string $cost, ProductType $productType)
    {
        $this->title       = $title;
        $this->sku         = $sku;
        $this->cost        = $cost;
        $this->productType = $productType;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getCost(): string
    {
        return $this->cost;
    }

    /**
     * @return ProductType
     */
    public function getProductType(): ProductType
    {
        return $this->productType;
    }
}
