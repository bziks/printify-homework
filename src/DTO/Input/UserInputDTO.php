<?php

namespace App\DTO\Input;


class UserInputDTO
{
    /** @var string */
    private $name;
    /** @var string */
    private $balance;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->balance = '100.00';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBalance(): string
    {
        return $this->balance;
    }
}
