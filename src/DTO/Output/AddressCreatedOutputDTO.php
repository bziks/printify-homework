<?php

namespace App\DTO\Output;


class AddressCreatedOutputDTO
{
    /** @var int */
    private $id;

    /**
     * ProductCreatedOutputDTO constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
