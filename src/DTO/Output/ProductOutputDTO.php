<?php

namespace App\DTO\Output;


class ProductOutputDTO
{
    /** @var int */
    private $id;
    /** @var string */
    private $title;
    /** @var string */
    private $sku;
    /** @var string */
    private $cost;
    /** @var string */
    private $productType;

    /**
     * ProductOutputDTO constructor.
     * @param int $id
     * @param string $title
     * @param string $sku
     * @param string $cost
     * @param string $productType
     */
    public function __construct(int $id, string $title, string $sku, string $cost, string $productType)
    {
        $this->id          = $id;
        $this->title       = $title;
        $this->sku         = $sku;
        $this->cost        = $cost;
        $this->productType = $productType;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getCost(): string
    {
        return $this->cost;
    }

    /**
     * @return string
     */
    public function getProductType(): string
    {
        return $this->productType;
    }
}
