<?php

namespace App\DTO\Output;


class UserCreatedOutputDTO
{
    /** @var int */
    private $id;

    /**
     * UserCreatedOutputDTO constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
