<?php

namespace App\DataFixtures;

use App\Entity\AddressType;
use App\Entity\Country;
use App\Entity\ProductType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $country1 = (new Country())
            ->setName('USA')
            ->setLabel('United States');
        $manager->persist($country1);

        $country2 = (new Country())
            ->setName('LV')
            ->setLabel('Latvia');
        $manager->persist($country2);

        $productType1 = (new ProductType())
            ->setName('mug')
            ->setLabel('Mug');
        $manager->persist($productType1);

        $productType2 = (new ProductType())
            ->setName('tshirt')
            ->setLabel('T-shirt');
        $manager->persist($productType2);

        $addressType1 = (new AddressType())
            ->setName('domestic')
            ->setLabel('Domestic');
        $manager->persist($addressType1);

        $addressType2 = (new AddressType())
            ->setName('international')
            ->setLabel('International');
        $manager->persist($addressType2);

        $manager->flush();
    }
}
