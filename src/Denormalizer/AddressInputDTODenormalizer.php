<?php

namespace App\Denormalizer;

use App\Denormalizer\Helper\ValidationTrait;
use App\DTO\Input\AddressInputDTO;
use App\Repository\CountryRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Serializer\Exception\BadMethodCallException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\ExtraAttributesException;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Exception\RuntimeException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;


class AddressInputDTODenormalizer implements DenormalizerInterface
{
    use ValidationTrait;

    /** @var CountryRepository */
    private $countryRepository;

    /**
     * AddressInputDTODenormalizer constructor.
     * @param CountryRepository $countryRepository
     */
    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @param array $context
     * @return AddressInputDTO
     * @throws EntityNotFoundException
     * @throws NonUniqueResultException
     */
    public function denormalize($data, $type, $format = null, array $context = []): AddressInputDTO
    {
        $this->validate($data, [
            'fullName' => 'required',
            'address' => 'required',
            'country' => 'required',
            'phone' => 'required',
            'city' => 'required',
        ]);

        if (!$country = $this->countryRepository->findByName($data['country'])) {
            throw new EntityNotFoundException("Country '{$data['country']}' not found");
        }

        return new AddressInputDTO(
            $data['fullName'],
            $data['address'],
            $country,
            $data['city'],
            $data['phone'],
            isset($data['region']) ? $data['region'] : null,
            isset($data['state']) ? $data['state'] : null,
            isset($data['zip']) ? $data['zip'] : null
        );
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return is_array($data) && ($type === AddressInputDTO::class);
    }
}
