<?php

namespace App\Denormalizer\Helper;


class ValidateEnum
{
    const REQUIRED = 'required';
    const JSON     = 'json';
    const ARRAY    = 'array';
    const NUMERIC  = 'numeric';
}
