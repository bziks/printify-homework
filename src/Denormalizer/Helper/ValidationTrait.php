<?php

namespace App\Denormalizer\Helper;


trait ValidationTrait
{
    /**
     * @param array $input
     * @param array $options
     */
    public function validate(array $input, array $options)
    {
        foreach ($options as $field => $rules) {
            if (is_string($rules)) {
                $rules = explode(',', $rules);
            }

            foreach ($rules as $rule) {
                switch ($rule) {
                    case ValidateEnum::REQUIRED:
                        if (!isset($input[$field]) || empty($input[$field])) {
                            throw new \InvalidArgumentException("Parameter {$field} is mandatory");
                        }
                        break;
                    case ValidateEnum::ARRAY:
                        if (isset($input[$field]) && !is_array($input[$field])) {
                            throw new \InvalidArgumentException($field . ' must be array');
                        }
                        break;
                    case ValidateEnum::NUMERIC:
                        if (isset($input[$field]) && !empty($input[$field]) && !is_numeric($input[$field])) {
                            throw new \InvalidArgumentException($field . ' must be numeric');
                        }
                        break;
                    case ValidateEnum::JSON:
                        if (isset($input[$field]) && !empty($input[$field])) {
                            $data = json_decode($input[$field]);
                            if (is_null($data)) {
                                throw new \InvalidArgumentException($field . ' must be valid json string');
                            }
                        }

                        break;
                }
            }
        }
    }
}
