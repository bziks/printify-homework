<?php

namespace App\Denormalizer;

use App\Denormalizer\Helper\ValidationTrait;
use App\DTO\Input\ProductInputDTO;
use App\Repository\ProductTypeRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;


class ProductInputDTODenormalizer implements DenormalizerInterface
{
    use ValidationTrait;

    /** @var ProductTypeRepository */
    private $productTypeRepository;

    /**
     * ProductInputDTODenormalizer constructor.
     * @param ProductTypeRepository $productTypeRepository
     */
    public function __construct(
        ProductTypeRepository $productTypeRepository
    )
    {
        $this->productTypeRepository = $productTypeRepository;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @param array $context
     * @return ProductInputDTO
     * @throws EntityNotFoundException
     * @throws NonUniqueResultException
     */
    public function denormalize($data, $type, $format = null, array $context = []): ProductInputDTO
    {
        $this->validate($data, [
            'title' => 'required',
            'sku' => 'required',
            'cost' => 'required',
            'productType' => 'required',
        ]);

        if (!$productType = $this->productTypeRepository->findByName($data['productType'])) {
            throw new EntityNotFoundException("Product type with name '{$data['productType']}' does not exist");
        }

        return new ProductInputDTO(
            $data['title'],
            $data['sku'],
            $data['cost'],
            $productType
        );
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return is_array($data) && ($type === ProductInputDTO::class);
    }
}
