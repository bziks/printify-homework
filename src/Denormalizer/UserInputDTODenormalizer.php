<?php

namespace App\Denormalizer;

use App\Denormalizer\Helper\ValidationTrait;
use App\DTO\Input\UserInputDTO;
use App\Exception\EntityAlreadyExistException;
use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;


class UserInputDTODenormalizer implements DenormalizerInterface
{
    use ValidationTrait;

    /** @var UserRepository */
    private $userRepository;

    /**
     * UserDTONormalizer constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @param array $context
     * @return UserInputDTO
     * @throws EntityAlreadyExistException
     * @throws NonUniqueResultException
     */
    public function denormalize($data, $type, $format = null, array $context = []): UserInputDTO
    {
        $this->validate($data, [
            'name' => 'required',
        ]);

        if ($this->userRepository->findByName($data['name'])) {
            throw new EntityAlreadyExistException("User with name '{$data['name']}' already exist");
        }

        return new UserInputDTO($data['name']);
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param null $format
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return is_array($data) && ($type === UserInputDTO::class);
    }
}
