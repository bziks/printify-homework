<?php


namespace App\Exception;

use LogicException;

/**
 * Class TransferObjectException
 * @package App\Exception
 */
class TransferObjectException extends LogicException
{

}