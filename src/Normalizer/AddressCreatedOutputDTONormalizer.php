<?php

namespace App\Normalizer;

use App\DTO\Output\AddressCreatedOutputDTO;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;


class AddressCreatedOutputDTONormalizer implements NormalizerInterface
{

    /**
     * @param AddressCreatedOutputDTO $object
     * @param null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
        ];
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof AddressCreatedOutputDTO;
    }
}
