<?php

namespace App\Normalizer;

use App\DTO\Output\ProductCreatedOutputDTO;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;


class ProductCreatedOutputDTONormalizer implements NormalizerInterface
{

    /**
     * @param ProductCreatedOutputDTO $object
     * @param null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
        ];
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof ProductCreatedOutputDTO;
    }
}
