<?php

namespace App\Normalizer;

use App\DTO\Output\ProductOutputDTO;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;


class ProductOutputDTONormalizer implements NormalizerInterface
{

    /**
     * @param ProductOutputDTO $object
     * @param null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|null
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'sku' => $object->getSku(),
            'cost' => (float)$object->getCost(),
            'productType' => $object->getProductType(),
        ];
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof ProductOutputDTO;
    }
}
