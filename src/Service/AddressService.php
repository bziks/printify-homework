<?php

namespace App\Service;

use App\DTO\Input\AddressInputDTO;
use App\DTO\Output\AddressCreatedOutputDTO;
use App\Entity\Address;
use App\Entity\AddressType;
use App\Entity\Country;
use App\Exception\EntityAlreadyExistException;
use App\Repository\AddressRepository;
use App\Repository\AddressTypeRepository;
use App\Repository\CountryRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;


class AddressService
{
    /** @var UserRepository */
    private $userRepository;
    /** @var AddressTypeRepository */
    private $addressTypeRepository;
    /** @var CountryRepository */
    private $countryRepository;
    /** @var AddressRepository */
    private $addressRepository;

    /**
     * AddressService constructor.
     * @param UserRepository $userRepository
     * @param AddressTypeRepository $addressTypeRepository
     * @param CountryRepository $countryRepository
     * @param AddressRepository $addressRepository
     */
    public function __construct(
        UserRepository $userRepository,
        AddressTypeRepository $addressTypeRepository,
        CountryRepository $countryRepository,
        AddressRepository $addressRepository
    ) {
        $this->userRepository = $userRepository;
        $this->addressTypeRepository = $addressTypeRepository;
        $this->countryRepository = $countryRepository;
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param int $userId
     * @param AddressInputDTO $addressInputDTO
     * @return AddressCreatedOutputDTO
     * @throws EntityNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(
        int $userId,
        AddressInputDTO $addressInputDTO
    ): AddressCreatedOutputDTO
    {
        if (!$user = $this->userRepository->find($userId)) {
            throw new EntityNotFoundException("User not found");
        }

        if ($user->getAddress()) {
            throw new EntityAlreadyExistException("User already have address");
        }

        $addressType = $this->getAddressType($addressInputDTO->getCountry());

        $address = (new Address())
            ->setUser($user)
            ->setAddressType($addressType)
            ->setFullName($addressInputDTO->getFullName())
            ->setAddress($addressInputDTO->getAddress())
            ->setCity($addressInputDTO->getCity())
            ->setCountry($addressInputDTO->getCountry())
            ->setPhone($addressInputDTO->getPhone())
            ->setRegion($addressInputDTO->getRegion())
            ->setState($addressInputDTO->getState())
            ->setZip($addressInputDTO->getZip());

        $this->addressRepository->save($address);

        return new AddressCreatedOutputDTO($address->getId());
    }

    /**
     * @param Country $country
     * @return AddressType
     * @throws EntityNotFoundException
     * @throws NonUniqueResultException
     */
    private function getAddressType(Country $country): AddressType
    {
        $name = AddressType::INTERNATIONAL;

        if ($country->getName() == Country::USA) {
            $name = AddressType::DOMESTIC;
        }

        $addressType = $this->addressTypeRepository->findByName($name);
        if (!$addressType) {
            throw new EntityNotFoundException("Address type '{$name}' not found");
        }

        return $addressType;
    }
}
