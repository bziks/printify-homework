<?php

namespace App\Service;

use App\DTO\Input\ProductInputDTO;
use App\DTO\Output\ProductCreatedOutputDTO;
use App\DTO\Output\ProductOutputDTO;
use App\Entity\Product;
use App\Exception\EntityAlreadyExistException;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;


class ProductService
{
    /** @var ProductRepository */
    private $productRepository;
    /** @var UserRepository */
    private $userRepository;

    /**
     * ProductService constructor.
     * @param ProductRepository $productRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        ProductRepository $productRepository,
        UserRepository $userRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $userId
     * @param ProductInputDTO $productDTO
     * @return ProductCreatedOutputDTO
     * @throws EntityNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(int $userId, ProductInputDTO $productDTO): ProductCreatedOutputDTO
    {
        if (!$user = $this->userRepository->find($userId)) {
            throw new EntityNotFoundException("User not found");
        }

        $product = (new Product())
            ->setTitle($productDTO->getTitle())
            ->setSku($productDTO->getSku())
            ->setCost($productDTO->getCost())
            ->setUserId($user)
            ->setProductType($productDTO->getProductType());

        try {
            $this->productRepository->save($product);
        } catch (UniqueConstraintViolationException $e) {
            throw new EntityAlreadyExistException("Product with sku '{$productDTO->getSku()}' already exist");
        }

        return new ProductCreatedOutputDTO($product->getId());
    }

    /**
     * @param int $userId
     * @return array
     * @throws EntityNotFoundException
     */
    public function getByUserId(int $userId): array
    {
        if (!$user = $this->userRepository->find($userId)) {
            throw new EntityNotFoundException("User not found");
        }

        $products = $this->productRepository->findByUser($user);
        $result = [];
        foreach ($products as $product) {
            $result[] = new ProductOutputDTO(
                $product->getId(),
                $product->getTitle(),
                $product->getSku(),
                $product->getCost(),
                $product->getProductType()->getLabel()
            );
        }

        return $result;
    }
}
