<?php

namespace App\Service;

use App\Exception\TransferObjectException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\MissingConstructorArgumentsException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;


class TransferObjectSerializer
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;

    /**
     * TransferObjectSerializer constructor.
     * @param SerializerInterface $serializer
     * @param DenormalizerInterface $denormalizer
     */
    public function __construct(
        SerializerInterface $serializer,
        DenormalizerInterface $denormalizer
    ) {
        $this->serializer = $serializer;
        $this->denormalizer = $denormalizer;
    }

    /**
     * @param string $json
     * @param string $objectClass
     *
     * @return object
     */
    public function deserializeJson(string $json, string $objectClass)
    {
        try {
            return $this->serializer->deserialize($json, $objectClass, 'json');
        } catch (MissingConstructorArgumentsException $exception) {
            throw new TransferObjectException($this->changeMissingConstructorParamMessage($exception->getMessage()));
        } catch (NotNormalizableValueException $exception) {
            throw new TransferObjectException($this->changeWrongTypeMessage($exception->getMessage()));
        } catch (NotEncodableValueException $exception) {
            throw new TransferObjectException("Request data are incorrect");
        }
    }

    /**
     * @param array $data
     * @param string $objectClass
     *
     * @return object|array
     * @throws ExceptionInterface
     */
    public function denormalizeArray(array $data, string $objectClass)
    {
        try {
            return $this->denormalizer->denormalize($data, $objectClass, 'array');
        } catch (MissingConstructorArgumentsException $exception) {
            throw new TransferObjectException($this->changeMissingConstructorParamMessage($exception->getMessage()));
        } catch (NotNormalizableValueException $exception) {
            throw new TransferObjectException($this->changeWrongTypeMessage($exception->getMessage()));
        }
    }

    /**
     * Overwrites default serializer MissingConstructorArgumentsException exception message.
     * Skips part about argument class, as it means nothing to end users.
     *
     * @param string $message
     *
     * @return string
     */
    private function changeMissingConstructorParamMessage(string $message): string
    {
        return sprintf('Function %s', substr($message, strpos($message, 'requires parameter')));
    }

    /**
     * Overwrites default serializer NotNormalizableValueException exception message.
     * Cuts out part about argument class, as it means nothing to end users.
     *
     * @param string $message
     *
     * @return string
     */
    private function changeWrongTypeMessage(string $message): string
    {
        return sprintf(
            '%s%s',
            substr($message, 0, strpos($message, 'for class')),
            substr($message, strpos($message, 'must be'))
        );
    }
}
