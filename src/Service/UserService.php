<?php

namespace App\Service;

use App\DTO\Input\UserInputDTO;
use App\DTO\Output\UserCreatedOutputDTO;
use App\Entity\Balance;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;


class UserService
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UserInputDTO $userDTO
     * @return UserCreatedOutputDTO
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(UserInputDTO $userDTO): UserCreatedOutputDTO
    {
        $balance = (new Balance())
            ->setAmount($userDTO->getBalance());

        $user = (new User())
            ->setName($userDTO->getName())
            ->setBalance($balance);

        $this->userRepository->save($user);

        return new UserCreatedOutputDTO($user->getId());
    }
}
